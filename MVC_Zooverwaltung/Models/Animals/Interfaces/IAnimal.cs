﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_Zooverwaltung.Models.Cages;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;

namespace MVC_Zooverwaltung.Models.Animals.Interfaces
{
    public interface IAnimal
    {
        int Id { get; }
        string Name { get; set; }
        ICage Cage { get; }
        FoodType FoodType { get; }

        
    }
}
