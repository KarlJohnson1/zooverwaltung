﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace MVC_Zooverwaltung.Models.Animals.Concretes
{
    public class Antilope : Herbivore
    {
        public Antilope(string Name, HerbivorousCage Cage) : base(Name, Cage) { }
    }
}