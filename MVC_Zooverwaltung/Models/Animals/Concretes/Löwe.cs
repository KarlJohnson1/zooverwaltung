﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;

namespace MVC_Zooverwaltung.Models.Animals.Concretes
{
    public class Löwe : Carnivore
    {
        public Löwe(string Name, CarnivoresCage Cage) : base(Name, Cage) { }
    }
}