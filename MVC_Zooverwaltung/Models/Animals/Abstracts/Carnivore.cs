﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace MVC_Zooverwaltung.Models.Animals.Abstracts
{
    public abstract class Carnivore : Animal, ICarnivore
    {
        protected CarnivoresCage _cage; 

        public override FoodType FoodType { get => FoodType.Carnivore; }
        public override ICage Cage { get => _cage; }
        public CarnivoresCage SpecificCage
        {
            get => _cage;
            set
            {
                _cage.Animals.Remove(this);
                value.Animals.Add(this);
                _cage = value;
            }
        }

        
        public Carnivore(string Name, CarnivoresCage Cage) : base(Name)
        {
            _cage = Cage;
        }        
    }
}