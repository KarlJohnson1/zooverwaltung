﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC_Zooverwaltung.Models.Animals.Abstracts
{
    public enum FoodType { Carnivore, Herbivore, Undetermined }

    public abstract class Animal : IAnimal
    {
        protected int _id;
        protected string _name;

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get => _id; set => _id = value; }
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Der Name muss zwischen 3 und 30 Zeichen lang sein")]
        public string Name { get => _name; set => _name = value; }        

        public abstract ICage Cage { get; }
        public abstract FoodType FoodType { get; }

        public override string ToString()
        {            
            return base.ToString() + " " + Name;
        }
        
        public Animal(string Name)
        {
            _name = Name;
        }
    }
}