﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;
using System.ComponentModel.DataAnnotations;

namespace MVC_Zooverwaltung.Models.Animals.Abstracts
{
    public class Herbivore : Animal, IHerbivore
    {
        protected HerbivorousCage _cage;

        public override FoodType FoodType { get => FoodType.Herbivore; }
        public override ICage Cage { get => _cage; }
        public HerbivorousCage SpecificCage
        {
            get => _cage;
            set
            {
                _cage.Animals.Remove(this);
                value.Animals.Add(this);
                _cage = value;
            }
        }

        public Herbivore(string Name, HerbivorousCage Cage) : base(Name)
        {
            _cage = Cage;
        }
    }
}