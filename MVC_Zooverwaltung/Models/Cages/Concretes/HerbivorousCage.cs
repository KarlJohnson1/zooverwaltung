﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;

namespace MVC_Zooverwaltung.Models.Cages.Concretes
{
    public class HerbivorousCage : Cage
    {
        private static int cageNr = 0;

        public override bool AnimalIsAddable(IAnimal animal)
        {
            if (animal is Herbivore && IsNotFull) return true;
            else return false;
        }

        public override bool AnimalIsAddable(Type animalTyp)
        {
            if (animalTyp == typeof(Antilope) && IsNotFull) return true;
            if (animalTyp == typeof(Gazelle) && IsNotFull) return true;
            else return false;
        }

        private HerbivorousCage() { }

        public HerbivorousCage(int capacity)
        {
            _name = "Herbivorous Cage Nr. " + ++cageNr;
            _capacity = capacity;
            _animals = new Collection<IAnimal>();
        }

    }
}