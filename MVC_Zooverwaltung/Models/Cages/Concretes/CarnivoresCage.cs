﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;
using MVC_Zooverwaltung.Models.Context;

namespace MVC_Zooverwaltung.Models.Cages.Concretes
{
    public class CarnivoresCage : Cage
    {
        private static int cageNr = 0;

        public override bool AnimalIsAddable(IAnimal animal)
        {
            if (animal is Carnivore && IsNotFull) return true;
            else return false;
        }

        public override bool AnimalIsAddable(Type animalTyp)
        {
            if (animalTyp == typeof(Löwe) && IsNotFull) return true;
            else return false;
        }


        private CarnivoresCage() { }

        public CarnivoresCage(int capacity)
        {
            _name = "Carnivores Cage Nr. " + ++cageNr;
            _capacity = capacity;
            _animals = new Collection<IAnimal>();
        }
    }
}