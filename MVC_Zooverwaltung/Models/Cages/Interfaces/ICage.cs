﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;

namespace MVC_Zooverwaltung.Models.Cages.Interfaces
{
    public interface ICage
    {
        int Id { get; }
        string Name { get; set; }
        int Capacity { get; }
        bool IsFull { get; }
        bool IsNotFull { get; }
        Collection<IAnimal> Animals { get; }

        bool AnimalIsAddable(IAnimal Animal);
        bool AnimalIsAddable(Type AnimalType);
        void AddAnimal(IAnimal Animal);
    }
}
