﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;

namespace MVC_Zooverwaltung.Models.Cages.Abstracts
{
    public abstract class Cage : ICage
    {        
        protected int _id;        
        protected string _name;
        protected int _capacity;
        protected Collection<IAnimal> _animals;

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get => _id; set => _id = value; }
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Der Name muss zwischen 3 und 30 Zeichen lang sein")]
        public string Name { get => _name; set => _name = value; }
        [Range(1, 50, ErrorMessage = "Die Kapazität muss zwischen 1 und 50 liegen")]        
        public int Capacity { get => _capacity; }
        public Collection<IAnimal> Animals { get => _animals; }

        public bool IsFull { get => _animals.Count >= _capacity; }
        public bool IsNotFull { get => _animals.Count < _capacity; }        

        public abstract bool AnimalIsAddable(IAnimal animal);
        public abstract bool AnimalIsAddable(Type animalTyp);

        public void AddAnimal(IAnimal animal)
        {
            if (AnimalIsAddable(animal))
            {
                _animals.Add(animal);
            }
            else throw new Exception();
        }

        public override string ToString()
        {
            return Name + "\n(Kapazität: " + _animals.Count + "/" + _capacity + ")";
        }

    }
}