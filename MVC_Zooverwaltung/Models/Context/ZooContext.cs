﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MVC_Zooverwaltung.Models.Animals;
using MVC_Zooverwaltung.Models.Animals.Abstracts;
using MVC_Zooverwaltung.Models.Animals.Concretes;
using MVC_Zooverwaltung.Models.Animals.Interfaces;
using MVC_Zooverwaltung.Models.Cages.Abstracts;
using MVC_Zooverwaltung.Models.Cages.Concretes;
using MVC_Zooverwaltung.Models.Cages.Interfaces;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace MVC_Zooverwaltung.Models.Context
{
    public class ZooContext : DbContext
    {
        public DbSet<CarnivoresCage> CarnivoresCages { get; set; }
        public DbSet<HerbivorousCage> HerbivorousCages { get; set; }
        
        public DbSet<Antilope> Antilopes { get; set; }
        public DbSet<Gazelle> Gazelles { get; set; }
        public DbSet<Löwe> Löwes { get; set; }
    }
}