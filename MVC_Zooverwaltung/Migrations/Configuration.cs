namespace MVC_Zooverwaltung.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;
    using MVC_Zooverwaltung.Models.Animals.Abstracts;
    using MVC_Zooverwaltung.Models.Animals.Concretes;
    using MVC_Zooverwaltung.Models.Animals.Interfaces;
    using MVC_Zooverwaltung.Models.Cages.Abstracts;
    using MVC_Zooverwaltung.Models.Cages.Concretes;
    using MVC_Zooverwaltung.Models.Cages.Interfaces;
    using System.Collections.ObjectModel;
    using System.Collections.Generic;

    internal sealed class Configuration : DbMigrationsConfiguration<MVC_Zooverwaltung.Models.Context.ZooContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(MVC_Zooverwaltung.Models.Context.ZooContext context)
        {
            if (System.Diagnostics.Debugger.IsAttached == false)
                System.Diagnostics.Debugger.Launch();

            for (int i = 1; i <= 3; i++)
            {
                var carnCage = context.CarnivoresCages.Find(i);
                if (carnCage == null) carnCage = new CarnivoresCage(10);
                context.CarnivoresCages.AddOrUpdate(carnCage);

                var herbCage = context.HerbivorousCages.Find(i);
                if (herbCage == null)
                {
                    herbCage = new HerbivorousCage(10);
                }
                context.HerbivorousCages.AddOrUpdate(herbCage);
            }

            //var l�wenDic = new Dictionary<int, string>();
            //l�wenDic.Add(1, "Leo");
            //l�wenDic.Add(2, "Lenhard");
            //l�wenDic.Add(3, "Larry");

            //foreach (var kvpL�we in l�wenDic)
            //{
            //    var l�we = context.L�wes.Find(kvpL�we.Key);
            //    if (l�we == null) l�we = new L�we(kvpL�we.Value, context.CarnivoresCages.Find(1));
            //    context.L�wes.AddOrUpdate(l�we);
            //}

            //var antilopenDic = new Dictionary<int, string>();
            //antilopenDic.Add(1, "Anna");
            //antilopenDic.Add(2, "Amelie");
            //antilopenDic.Add(3, "Ankathrin");

            //foreach (var kvpAntilope in antilopenDic)
            //{
            //    var antilope = context.Antilopes.Find(kvpAntilope.Key);
            //    if (antilope == null) antilope = new Antilope(kvpAntilope.Value, context.HerbivorousCages.Find(1));
            //    context.Antilopes.AddOrUpdate(antilope);
            //}

            //var gazellenDic = new Dictionary<int, string>();
            //gazellenDic.Add(1, "Gertrud");
            //gazellenDic.Add(2, "Gertlinde");
            //gazellenDic.Add(3, "Gaby");

            //foreach (var kvpGazelle in gazellenDic)
            //{
            //    var gazelle = context.Gazelles.Find(kvpGazelle.Key);
            //    if (gazelle == null) gazelle = new Gazelle(kvpGazelle.Value, context.HerbivorousCages.Find(2));
            //    context.Gazelles.AddOrUpdate(gazelle);
            //}





        }
    }
}
