namespace MVC_Zooverwaltung.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Inheritance : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Antilopes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SpecificCage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HerbivorousCages", t => t.SpecificCage_Id)
                .Index(t => t.SpecificCage_Id);
            
            CreateTable(
                "dbo.HerbivorousCages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.CarnivoresCages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Gazelles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SpecificCage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HerbivorousCages", t => t.SpecificCage_Id)
                .Index(t => t.SpecificCage_Id);
            
            CreateTable(
                "dbo.Löwe",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        SpecificCage_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CarnivoresCages", t => t.SpecificCage_Id)
                .Index(t => t.SpecificCage_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Löwe", "SpecificCage_Id", "dbo.CarnivoresCages");
            DropForeignKey("dbo.Gazelles", "SpecificCage_Id", "dbo.HerbivorousCages");
            DropForeignKey("dbo.Antilopes", "SpecificCage_Id", "dbo.HerbivorousCages");
            DropIndex("dbo.Löwe", new[] { "SpecificCage_Id" });
            DropIndex("dbo.Gazelles", new[] { "SpecificCage_Id" });
            DropIndex("dbo.Antilopes", new[] { "SpecificCage_Id" });
            DropTable("dbo.Löwe");
            DropTable("dbo.Gazelles");
            DropTable("dbo.CarnivoresCages");
            DropTable("dbo.HerbivorousCages");
            DropTable("dbo.Antilopes");
        }
    }
}
