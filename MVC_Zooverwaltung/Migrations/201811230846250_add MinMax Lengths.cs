namespace MVC_Zooverwaltung.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addMinMaxLengths : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Antilopes", "Name", c => c.String(maxLength: 30));
            AlterColumn("dbo.HerbivorousCages", "Name", c => c.String(maxLength: 30));
            AlterColumn("dbo.CarnivoresCages", "Name", c => c.String(maxLength: 30));
            AlterColumn("dbo.Gazelles", "Name", c => c.String(maxLength: 30));
            AlterColumn("dbo.Löwe", "Name", c => c.String(maxLength: 30));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Löwe", "Name", c => c.String());
            AlterColumn("dbo.Gazelles", "Name", c => c.String());
            AlterColumn("dbo.CarnivoresCages", "Name", c => c.String());
            AlterColumn("dbo.HerbivorousCages", "Name", c => c.String());
            AlterColumn("dbo.Antilopes", "Name", c => c.String());
        }
    }
}
